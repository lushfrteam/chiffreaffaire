<?php
include("./config.php");
require_once("./Functions.php");
echo "<BR>Upload ordre OGONE<BR>";
$upload_table=$_GET['fichier'];

if( isset($_POST['upload']) ) // si formulaire soumis
{
    $nom_table=$_POST['table'];
    global $content_dir;
    global $nom_fichier_ogone;
    global $rep_upload;
    $nom_fichier_complet=$content_dir.$nom_fichier_ogone;
    $nom_fichier=$_FILES['nom_fichier']['name'];
    $tmp_file = $_FILES['nom_fichier']['tmp_name'];
    $name_file = $_FILES['fichier']['name'];
    
   if( !move_uploaded_file($tmp_file, $nom_fichier_complet) )
    
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }
    echo "Le fichier a bien été uploadé : ".$nom_fichier;
    
    /**** Connexion bdd ****/
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
 //  echo 'Connected successfully';
    
 /***************************/
 /* On cree la table  OGONE */
 /***************************/
 
  $query = "CREATE TABLE `ogone` (
                                `Id` varchar(32) NOT NULL,
				`REF` int(11) NOT NULL,
				`ORDER` date NOT NULL DEFAULT '0000-00-00',
                                `STATUS` int(11) NOT NULL,
                                `LIB` varchar(32) NOT NULL DEFAULT '' ,
                                `ACCEPT` int(11) NOT NULL,
                                `PAYDATE` date NOT NULL DEFAULT '0000-00-00',
                                `CIE` varchar(32) NOT NULL DEFAULT '',
                                `NAME` varchar(32) NOT NULL DEFAULT '' ,
                                `COUNTRY`varchar(32) NOT NULL DEFAULT '',
                                `TOTAL` decimal(15,2) NOT NULL DEFAULT '0.0000',
                                `CUR` varchar(32) NOT NULL DEFAULT '' ,
                                `METHODE` varchar(32) NOT NULL DEFAULT '' ,
                                `BRAND` varchar(32) NOT NULL DEFAULT '',
                                `CARD` varchar(32) NOT NULL DEFAULT '' ,
                                `STRUCT` int(11) NOT NULL,
            PRIMARY KEY (`Id`),
            KEY `Id_idx` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 

   $result= MYSQL_QUERY($query);
   
// $pdo->prepare($query)->execute();

/**************************/
/*** import csv ogone *****/
/**************************/
 
 $query = "LOAD DATA INFILE '".$nom_fichier_complet."' 
            INTO TABLE `ogone`
             CHARACTER SET LATIN1
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r\n'
            IGNORE 1 LINES
            ( Id,REF,@ORDER,STATUS,LIB,ACCEPT,
              @`PAYDATE`,CIE,NAME,COUNTRY,@`TOTAL`,CUR,METHODE,BRAND,CARD,STRUCT)
            SET `ORDER`= STR_TO_DATE(@`ORDER`,'%d/%m/%Y'),
                `PAYDATE` = STR_TO_DATE(@`PAYDATE`,'%d/%m/%Y'),
                `TOTAL` = REPLACE(@`TOTAL`, ',', '.')
              
            ";
$result=mysql_query($query);
mysql_close($mysql_link);

?>
<br><center><input type="button" value="fermer" onclick="javascript:window.opener.location.href='./';window.close();"></center>
<?php

        
}
else { 

echo $upload_table."<br>";
echo '<form method="post" enctype="multipart/form-data" action="upload_ogone.php">';
echo '<label for="mon_fichier">Fichier (tous formats | max. 3 Mo) :</label><br />';
echo '<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />';
echo '<input type="hidden" name="table" value='.$upload_table.' />';
echo '<input type="file" name="nom_fichier" id="nom_fichier" />';
echo '<input type="submit" name="upload" value="Uploader">';
echo '</form>';
}

?>