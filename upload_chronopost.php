<?php
include("./config.php");
require_once("./Functions.php");

$upload_table=$_GET['fichier'];

if( isset($_POST['upload']) ) // si formulaire soumis
{
    $nom_table=$_POST['table'];
    global $content_dir;
    global $nom_fichier_chronopost;
    global $rep_upload;
    $nom_fichier_complet=$content_dir.$nom_fichier_chronopost;
    $nom_fichier=$_FILES['nom_fichier']['name'];
    $tmp_file = $_FILES['nom_fichier']['tmp_name'];
    $name_file = $_FILES['fichier']['name'];
    
    if( !move_uploaded_file($tmp_file, $nom_fichier_complet) )
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }
    echo "Le fichier a bien été uploadé : ".$nom_fichier;
    
    /**** Connexion bdd ****/
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
 //  echo 'Connected successfully';
    
    /****************************/
 /* On cree la table  Chrono */
 /****************************/
 
  $query = "CREATE TABLE `chronopost` (
  
  `No_de_ligne` int(11) NOT NULL,
  `No_de_compte` int(11) NOT NULL,
  `N_de_sous_compte` int(11) NOT NULL,
  `Filtre_appliquer` varchar(255) NOT NULL,
  `Solution` varchar(32) NOT NULL,
  `Referenc_envoi` varchar(32) NOT NULL,
  `reference_expediteur` varchar(32) NOT NULL,
  `Remis_le` datetime NOT NULL DEFAULT '0000-00-00',
  `Date_et_heure_de_l_evenement` datetime NOT NULL DEFAULT '0000-00-00',
  `Evenement` varchar(255) NOT NULL,
  `Complement_d_information` varchar(255) NOT NULL,
  `Lieu` varchar(50) NOT NULL,
  `Colis_en_retour` varchar(5) NOT NULL,
  `Livraison_le_samedi` varchar(5) NOT NULL,
  `Reference_destinataire` varchar(32) NOT NULL,
  `Raison_sociale_destinataire` varchar(32) NOT NULL,
  `Suite_raison_sociale_destinataire` varchar(32) NOT NULL,
  `Nom_et_prenom_destinataire` varchar(255) NOT NULL,
  `Adresse_destinataire` varchar(255) NOT NULL,
  `Suite_adresse_destinataire` varchar(255) NOT NULL,
  `Code_postal_destinataire` int(11) NOT NULL,
  `Ville_destinataire` varchar(255) NOT NULL,
  `Pays_destinataire` varchar(5) NOT NULL,
  `Telephone_destinataire` varchar(20) NOT NULL,
  `Mail_destinataire` varchar(255) NOT NULL,
  `Raison_sociale_expediteur` varchar(32) NOT NULL,
  `Suite_raison_sociale_expediteur` varchar(32) NOT NULL,
  `Nom_et_prenom_expediteur` varchar(255) NOT NULL,
  `Adresse_expediteur` varchar(255) NOT NULL,
  `Suite_adresse_expediteur` varchar(255) NOT NULL,
  `Code_postal_expediteur` int(11) NOT NULL,
  `Ville_expediteur` varchar(255) NOT NULL,
  `Pays_expediteur` varchar(5) NOT NULL,
  `Telephone_expediteur` varchar(20) NOT NULL,
  `Mail_expediteur` varchar(50) NOT NULL,
  `cdes` varchar(255) NOT NULL,
  
  PRIMARY KEY (`No_de_ligne`),
            KEY `No_de_ligne_idx` (`No_de_ligne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 $result= MYSQL_QUERY($query);
 
/***************************/
/*** import csv chrono *****/
/***************************/
 
 $query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/upload/chronopost.csv' 
            INTO TABLE `chronopost`
            CHARACTER SET LATIN1
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r\n'
            IGNORE 1 LINES
            (
                No_de_ligne,No_de_compte,N_de_sous_compte,
                Filtre_appliquer,Solution,Referenc_envoi,reference_expediteur,
                @Remis_le,@Date_et_heure_de_l_evenement,Evenement,
                Complement_d_information,Lieu,Colis_en_retour,
                Livraison_le_samedi,Reference_destinataire,
                Raison_sociale_destinataire,Suite_raison_sociale_destinataire,
                Nom_et_prenom_destinataire,Adresse_destinataire,
                Suite_adresse_destinataire,Code_postal_destinataire,
                Ville_destinataire,Pays_destinataire,Telephone_destinataire,
                Mail_destinataire,Raison_sociale_expediteur,
                Suite_raison_sociale_expediteur,Nom_et_prenom_expediteur,
                Adresse_expediteur,Suite_adresse_expediteur,
                Code_postal_expediteur,Ville_expediteur,Pays_expediteur,
                Telephone_expediteur,Mail_expediteur,cdes)
            
            SET `Remis_le`= STR_TO_DATE(@`Remis_le`,'%d/%m/%Y'),
                `Date_et_heure_de_l_evenement` = STR_TO_DATE(@`Date_et_heure_de_l_evenement`,'%d/%m/%Y') 
              
            ";
 $n=mysql_query($query);

?>
<br><center><input type="button" value="fermer" onclick="javascript:window.opener.location.href='./';window.close();"></center>
<?php

        
}
else { 

echo $upload_table."<br>";
echo '<form method="post" enctype="multipart/form-data" action="upload_chronopost.php">';
echo '<label for="mon_fichier">Fichier (tous formats | max. 3 Mo) :</label><br />';
echo '<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />';
echo '<input type="hidden" name="table" value='.$upload_table.' />';
echo '<input type="file" name="nom_fichier" id="nom_fichier" />';
echo '<input type="submit" name="upload" value="Uploader">';
echo '</form>';
}

?>