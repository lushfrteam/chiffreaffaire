<?php
include("./config.php");
require_once("./Functions.php");

$upload_table=$_GET['fichier'];

if( isset($_POST['upload']) ) // si formulaire soumis
{
    $nom_table=$_POST['table'];
    global $content_dir;
    global $nom_fichier_fraisport;
    global $rep_upload;
    $nom_fichier_complet=$content_dir.$nom_fichier_remboursement;
    $nom_fichier=$_FILES['nom_fichier']['name'];
    $tmp_file = $_FILES['nom_fichier']['tmp_name'];
    $name_file = $_FILES['fichier']['name'];
    
    if( !move_uploaded_file($tmp_file, $nom_fichier_complet) )
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }
    echo "Le fichier a bien été uploadé : ".$nom_fichier;
    
    /**** Connexion bdd ****/
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
 //  echo 'Connected successfully';
    
    /***********************************/
    /* On cree la table  remboursement */
    /***********************************/
 
  $query = "CREATE TABLE `remboursement` (
  `Id` int(5) NOT NULL auto_increment,
  `Date` date NOT NULL DEFAULT '0000-00-00',
  `Order` int(11) NOT NULL,
  `Montant_TTC` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Taxes` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Montant_HT` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Paypal-Ogone` varchar(32) NOT NULL,
  `Remboursement_Transport` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Remboursement_Produit` decimal(15,2) NOT NULL DEFAULT '0.00',
  `Detail` varchar(50) NOT NULL,
   
  PRIMARY KEY (`Id`),
            KEY `Id_idx` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 $result= MYSQL_QUERY($query);
 
/***************************/
/*** import csv Frais de port *****/
/***************************/
 
 $query = "LOAD DATA INFILE '".$nom_fichier_complet."' 
            INTO TABLE `remboursement`
            CHARACTER SET LATIN1
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r\n'
            IGNORE 1 LINES
            (@`Date`, `Order`, @Montant_TTC, @Taxes, @Montant_HT,`Paypal-Ogone`,
            @Remboursement_Transport,@Remboursement_Produit,`Detail`)
            SET `Date`= STR_TO_DATE(@`Date`,'%d/%m/%Y'), 
            `Montant_TTC`= REPLACE(@`Montant_TTC`, ',', '.'),
            `Taxes`= REPLACE(@`Taxes`, ',', '.'),
            `Montant_HT`= REPLACE(@`Montant_HT`, ',', '.'),
            `Remboursement_Transport`= REPLACE(@`Remboursement_Transport`, ',', '.'),
            `Remboursement_Produit`= REPLACE(@`Remboursement_Produit`, ',', '.')
            ";
 $n=mysql_query($query);
 
?>


<br><center><input type="button" value="fermer" onclick="javascript:window.opener.location.href='./';window.close();"></center>
<?php

        
}
else { 

echo $upload_table."<br>Upload Remboursement<BR>";
echo '<form method="post" enctype="multipart/form-data" action="upload_remboursement.php">';
echo '<label for="mon_fichier">Fichier (tous formats | max. 3 Mo) :</label><br />';
echo '<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />';
echo '<input type="hidden" name="table" value='.$upload_table.' />';
echo '<input type="file" name="nom_fichier" id="nom_fichier" />';
echo '<input type="submit" name="upload" value="Uploader">';
echo '</form>';
}

?>