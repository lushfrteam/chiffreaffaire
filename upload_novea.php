<?php
include("./config.php");
require_once("./Functions.php");

$upload_table=$_GET['fichier'];

if( isset($_POST['upload']) ) // si formulaire soumis
{
    $nom_table=$_POST['table'];
    global $content_dir;
    global $nom_fichier_novea;
    global $rep_upload;
    $nom_fichier_complet=$content_dir.$nom_fichier_novea;
    $nom_fichier=$_FILES['nom_fichier']['name'];
    $tmp_file = $_FILES['nom_fichier']['tmp_name'];
    $name_file = $_FILES['fichier']['name'];
    if( !move_uploaded_file($tmp_file, $nom_fichier_complet) )
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }
    echo "Le fichier a bien été uploadé : ".$nom_fichier;
    
    /**** Connexion bdd ****/
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
 //  echo 'Connected successfully';
    
    /****************************/
    /* On cree la table  Novea  */
    /****************************/
 
  $query = "CREATE TABLE `novea` (
  `NumCommande` varchar(32)NOT NULL,
  `CodeClient` varchar(32) NOT NULL,
  `Nom` varchar(32) NOT NULL,
  `Service` varchar(50) NOT NULL,
  `Demandeur` varchar(50) NOT NULL,
  `DonneurOrdre` varchar(50) NOT NULL,
  `Expediteur` varchar(50) NOT NULL,
  `CodePostalExp` int(11) NOT NULL,
  `VilleExp` varchar(32) NOT NULL,  
  `Destinataire` varchar(255) NOT NULL,
  `CodePostalDest` int(11) NOT NULL,
  `VilleDest` varchar(255) NOT NULL,
  `Retour` varchar(32) NOT NULL,
  `CodeImputation` varchar(32) NOT NULL,
  `NumeroCommandeClient` varchar(32) NOT NULL,
  `NumeroCommandeClientSecondaire` varchar(32) NOT NULL,
  `MinutesAttente` int(5) NOT NULL,
  `MontantTotal` decimal(15,2) NOT NULL DEFAULT '0.00',
  `MontantDeBase` decimal(15,2) NOT NULL DEFAULT '0.00',
  `DateLivraison`  date NOT NULL DEFAULT '0000-00-00',
  `HeureLivraison` time NOT NULL DEFAULT '00:00:00',
  `ContactExpediteur` varchar(50) NOT NULL,
  `ContactDestinataire` varchar(255) NOT NULL,
  `Options` varchar(50) NOT NULL,
  `Messagerie` int(5) NOT NULL,
  `ConsommationCo2` decimal(15,2) NOT NULL DEFAULT '0.00',
  `EconomieCo2` decimal(15,2) NOT NULL DEFAULT '0000.00',
   PRIMARY KEY (`NumCommande`),
           KEY `NumCommande_idx` (`NumCommande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 $result= MYSQL_QUERY($query);
 
/***************************/
/*** import csv Novea  *****/
/***************************/
 
 $query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/upload/novea.csv' 
            INTO TABLE `novea`
             CHARACTER SET LATIN1
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r\n'
            IGNORE 1 LINES
            (
          
    NumCommande,CodeClient,Nom,Service,Demandeur,DonneurOrdre,Expediteur,
    CodePostalExp,VilleExp,Destinataire,CodePostalDest,VilleDest,Retour,
    CodeImputation,NumeroCommandeClient,NumeroCommandeClientSecondaire,
    MinutesAttente,@MontantTotal,@MontantDeBase,@DateLivraison,
    HeureLivraison,ContactExpediteur,ContactDestinataire,`Options`,
    Messagerie,@ConsommationCo2,@EconomieCo2)
            
    SET `DateLivraison`= STR_TO_DATE(@`DateLivraison`,'%d/%m/%Y'),
    `MontantTotal` = REPLACE(@`MontantTotal`, ',', '.'),              
    `MontantDeBase` = REPLACE(@`MontantDeBase`, ',', '.'),    
    `ConsommationCo2` = REPLACE(@`ConsommationCo2`, ',', '.'),              
    `EconomieCo2` = REPLACE(@`EconomieCo2`, ',', '.') 
";
 $n=mysql_query($query);

?>
<br><center><input type="button" value="fermer" onclick="javascript:window.opener.location.href='./';window.close();"></center>
<?php

        
}
else { 

echo $upload_table."<br>";
echo '<form method="post" enctype="multipart/form-data" action="upload_novea.php">';
echo '<label for="mon_fichier">Fichier (tous formats | max. 3 Mo) :</label><br />';
echo '<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />';
echo '<input type="hidden" name="table" value='.$upload_table.' />';
echo '<input type="file" name="nom_fichier" id="nom_fichier" />';
echo '<input type="submit" name="upload" value="Uploader">';
echo '</form>';
}

?>