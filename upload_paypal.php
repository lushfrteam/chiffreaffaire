<?php
include("./config.php");
require_once("./Functions.php");

$upload_table=$_GET['fichier'];

if( isset($_POST['upload']) ) // si formulaire soumis
{
    $nom_table=$_POST['table'];
    global $content_dir;
    global $nom_fichier_paypal;
    global $rep_upload;
    $nom_fichier_complet=$content_dir.$nom_fichier_paypal;
    $nom_fichier=$_FILES['nom_fichier']['name'];
    $tmp_file = $_FILES['nom_fichier']['tmp_name'];
    $name_file = $_FILES['fichier']['name'];
    
   if( !move_uploaded_file($tmp_file, $nom_fichier_complet) )
    
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }
    echo "Le fichier a bien été uploadé : ".$nom_fichier;
    
    /**** Connexion bdd ****/
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
 //  echo 'Connected successfully';
    
 /****************************/
 /* On cree la table  Paypal */
 /****************************/
 
  $query = "CREATE TABLE `paypal` (
                                `Id` int(11) NOT NULL auto_increment,
                                `Date` date NOT NULL DEFAULT '0000-00-00',
				`PaiementRecu` int(11) NOT NULL,
				`MontantRecu` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `Tarifs` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `RemboursementsEnvoyes` int(11) NOT NULL ,
                                `MontantRembourse` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `CommissionsRemboursees` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `PaiementsEnvoyes` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `MontantEnvoye` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `AutresDebits`decimal(15,2) NOT NULL DEFAULT '0.00',
                                `AutresCredits` decimal(15,2) NOT NULL DEFAULT '0.00',
                                `MontantNet` decimal(15,2) NOT NULL DEFAULT '0.00',
                                 
    PRIMARY KEY (`Id`),
    KEY `Id_idx` (`Id`)
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 

   $result= MYSQL_QUERY($query);
   
// $pdo->prepare($query)->execute();

/**************************/
/*** import csv paypal ****/
/**************************/
 
 $query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/upload/paypal.csv' 
            INTO TABLE `paypal`
            CHARACTER SET LATIN1
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r\n'
            IGNORE 1 LINES
                ( @`Date`,PaiementRecu,@`MontantRecu`,@`Tarifs`,
                RemboursementsEnvoyes,@`MontantRembourse`,@`CommissionsRemboursees`,
                PaiementsEnvoyes,@`MontantEnvoye`,@`AutresDebits`,@`AutresCredits`,
                @`MontantNet`)
            SET `Date`= STR_TO_DATE(@`Date`,'%d/%m/%Y'), 
                `MontantRecu` = REPLACE(@`MontantRecu`, ',', '.'),
                `CommissionsRemboursees` = REPLACE(@`CommissionsRemboursees`, ',', '.'),
                `MontantEnvoye` = REPLACE(@`MontantEnvoye`, ',', '.'),
                `AutresDebits` = REPLACE(@`AutresDebits`, ',', '.'),
                `AutresCredits` = REPLACE(@`AutresCredits`, ',', '.'),
                `MontantNet` = REPLACE(@`MontantNet`, ',', '.')   
            ";

 $result=mysql_query($query);
mysql_close($mysql_link);

?>
<br><center><input type="button" value="fermer" onclick="javascript:window.opener.location.href='./';window.close();"></center>
<?php

        
}
else { 

echo $upload_table."<br>";
echo '<form method="post" enctype="multipart/form-data" action="upload_paypal.php">';
echo '<label for="mon_fichier">Fichier (tous formats | max. 3 Mo) :</label><br />';
echo '<input type="hidden" name="MAX_FILE_SIZE" value="3145728" />';
echo '<input type="hidden" name="table" value='.$upload_table.' />';
echo '<input type="file" name="nom_fichier" id="nom_fichier" />';
echo '<input type="submit" name="upload" value="Uploader">';
echo '</form>';
}

?>