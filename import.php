
<html> 
<head> 
<title>Importer un fichier texte dans une bdd MySQL</title> 
</head> 
<body> 
<h2>Importer un fichier texte dans une bdd MySQL</h2> 

<?
 
// SERVEUR SQL
$sql_serveur="localhost:8889";
$sql_port="8889";
$sql_server="localhost";

// LOGIN SQL
$sql_user="root";

// MOT DE PASSE SQL
$sql_password="root";

// NOM DE LA BASE DE DONNEES
$sql_database="cadrage";

// NOM DE LA TABLE DE DONNEES
$sql_table="order";


 /* Connexion bdd */
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
   echo 'Connected successfully';
 
 /* On cree la table */
  $query = "CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL DEFAULT '',
  `Products` int(11) NOT NULL,
  `Sub_Total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Shipping` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Payment` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Other` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Coupon` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Discount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Voucher` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Taxes` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `Total_Inc_Taxes` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `currency` varchar(3) NOT NULL,  
  `payment_method` varchar(128) NOT NULL DEFAULT '',
  `shipping_method_id` varchar(25) DEFAULT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  `date_shipping` date NOT NULL DEFAULT '0000-00-00',
  
  PRIMARY KEY (`order_id`),
  KEY `order_id_idx` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
$result= MYSQL_QUERY($query);



define('CSV_PATH','./'); // specify CSV file path
$csv_file = "commandes.csv"; // Name of your CSV file

  if (file_exists($csv_file)){
        $fp = fopen($csv_file, "r");
        echo "Fichier Ouvert<br>";
        }
    else { /* le fichier n'existe pas */
        echo "Fichier introuvable !<br>Importation stoppée.";
        exit();
    }
ini_set("auto_detect_line_endings", true);

/********************/
/** import order ****/
/********************/
$query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/commandes.csv' 
            INTO TABLE `order`
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r'
            IGNORE 1 LINES
            (order_id,firstname,lastname,shipping_firstname,shipping_lastname,
            Products,Sub_Total,Shipping,Payment,Other,Coupon,Discount,
            Voucher,Taxes,Total_Inc_Taxes,currency,payment_method,
            shipping_method_id,
            @date_added,
            @date_shipping ) 
            SET date_added = STR_TO_DATE(@date_added,'%d/%m/%Y'),
                date_shipping = STR_TO_DATE(@date_shipping,'%d/%m/%Y')
            ";
    
$n=mysql_query($query);
fclose($fp);
mysql_close($mysql_link);


try
{
    // On se connecte à MySQL
      $pdo = new PDO('mysql:host=localhost;port=8889;dbname=cadrage','root','root');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
      $rs = $pdo->query('SELECT * FROM `order`');
           
    echo 'Il y a '.$rs->rowcount().' enregistrement(s) dans Order.<br>';


//echo "File data successfully imported to database!!:<BR> ";

 /* Connexion bdd */
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
 //  echo 'Connected successfully';
/***************************/
 /* On cree la table  OGONE */
 /***************************/
 
  $query = "CREATE TABLE `ogone` (
                                `Id` varchar(32) NOT NULL,
				`REF` int(11) NOT NULL,
				`ORDER` date NOT NULL DEFAULT '0000-00-00',
                                `STATUS` int(11) NOT NULL,
                                `LIB` varchar(32) NOT NULL DEFAULT '' ,
                                `ACCEPT` int(11) NOT NULL,
                                `PAYDATE` date NOT NULL DEFAULT '0000-00-00',
                                `CIE` varchar(32) NOT NULL DEFAULT '',
                                `NAME` varchar(32) NOT NULL DEFAULT '' ,
                                `COUNTRY`varchar(32) NOT NULL DEFAULT '',
                                `TOTAL` decimal(15,4) NOT NULL DEFAULT '0.0000',
                                `CUR` varchar(32) NOT NULL DEFAULT '' ,
                                `METHODE` varchar(32) NOT NULL DEFAULT '' ,
                                `BRAND` varchar(32) NOT NULL DEFAULT '',
                                `CARD` varchar(32) NOT NULL DEFAULT '' ,
                                `STRUCT` int(11) NOT NULL,
            PRIMARY KEY (`REF`),
            KEY `REF_idx` (`REF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 

   $result= MYSQL_QUERY($query);
   
// $pdo->prepare($query)->execute();

/**************************/
/*** import csv ogone *****/
/**************************/
 
 $query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/ogone.csv' 
            INTO TABLE `ogone`
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r'
            IGNORE 1 LINES
            ( Id,REF,@ORDER,STATUS,LIB,ACCEPT,
              @`PAYDATE`,CIE,NAME,COUNTRY,TOTAL,CUR,METHODE,BRAND,CARD,STRUCT)
            SET `ORDER`= STR_TO_DATE(@`ORDER`,'%d/%m/%Y'),
                `PAYDATE` = STR_TO_DATE(@`PAYDATE`,'%d/%m/%Y') 
              
            ";
 $n=mysql_query($query);

 try
{
    // On se connecte à MySQL
      $pdo = new PDO('mysql:host=localhost;port=8889;dbname=cadrage','root','root');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
      $rs = $pdo->query('SELECT * FROM `ogone`');
           
    echo 'Il y a '.$rs->rowcount().' enregistrement(s) dans Ogone.<br>';

    
 /****************************/
 /* On cree la table  Chrono */
 /****************************/
 
  $query = "CREATE TABLE `chronopost` (
  
  `No_de_ligne` int(11) NOT NULL,
  `No_de_compte` int(11) NOT NULL,
  `N_de_sous_compte` int(11) NOT NULL,
  `Filtre_appliquer` varchar(255) NOT NULL,
  `Solution` varchar(32) NOT NULL,
  `Referenc_envoi` varchar(32) NOT NULL,
  `reference_expediteur` varchar(32) NOT NULL,
  `Remis_le` datetime NOT NULL DEFAULT '0000-00-00',
  `Date_et_heure_de_l_evenement` datetime NOT NULL DEFAULT '0000-00-00',
  `Evenement` varchar(255) NOT NULL,
  `Complement_d_information` varchar(255) NOT NULL,
  `Lieu` varchar(50) NOT NULL,
  `Colis_en_retour` varchar(5) NOT NULL,
  `Livraison_le_samedi` varchar(5) NOT NULL,
  `Reference_destinataire` varchar(32) NOT NULL,
  `Raison_sociale_destinataire` varchar(32) NOT NULL,
  `Suite_raison_sociale_destinataire` varchar(32) NOT NULL,
  `Nom_et_prenom_destinataire` varchar(255) NOT NULL,
  `Adresse_destinataire` varchar(255) NOT NULL,
  `Suite_adresse_destinataire` varchar(255) NOT NULL,
  `Code_postal_destinataire` int(11) NOT NULL,
  `Ville_destinataire` varchar(255) NOT NULL,
  `Pays_destinataire` varchar(5) NOT NULL,
  `Telephone_destinataire` varchar(20) NOT NULL,
  `Mail_destinataire` varchar(255) NOT NULL,
  `Raison_sociale_expediteur` varchar(32) NOT NULL,
  `Suite_raison_sociale_expediteur` varchar(32) NOT NULL,
  `Nom_et_prenom_expediteur` varchar(255) NOT NULL,
  `Adresse_expediteur` varchar(255) NOT NULL,
  `Suite_adresse_expediteur` varchar(255) NOT NULL,
  `Code_postal_expediteur` int(11) NOT NULL,
  `Ville_expediteur` varchar(255) NOT NULL,
  `Pays_expediteur` varchar(5) NOT NULL,
  `Telephone_expediteur` varchar(20) NOT NULL,
  `Mail_expediteur` varchar(50) NOT NULL,
  `cdes` varchar(255) NOT NULL,
  
  PRIMARY KEY (`No_de_ligne`),
            KEY `No_de_ligne_idx` (`No_de_ligne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;";
 $result= MYSQL_QUERY($query);
 
/***************************/
/*** import csv chrono *****/
/***************************/
 
 $query = "LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/chrono.csv' 
            INTO TABLE `chronopost`
            FIELDS TERMINATED BY ';' 
            ENCLOSED BY '' 
            LINES TERMINATED BY '\r'
            IGNORE 1 LINES
            (
                No_de_ligne,No_de_compte,N_de_sous_compte,
                Filtre_appliquer,Solution,Referenc_envoi,reference_expediteur,
                @Remis_le,@Date_et_heure_de_l_evenement,Evenement,
                Complement_d_information,Lieu,Colis_en_retour,
                Livraison_le_samedi,Reference_destinataire,
                Raison_sociale_destinataire,Suite_raison_sociale_destinataire,
                Nom_et_prenom_destinataire,Adresse_destinataire,
                Suite_adresse_destinataire,Code_postal_destinataire,
                Ville_destinataire,Pays_destinataire,Telephone_destinataire,
                Mail_destinataire,Raison_sociale_expediteur,
                Suite_raison_sociale_expediteur,Nom_et_prenom_expediteur,
                Adresse_expediteur,Suite_adresse_expediteur,
                Code_postal_expediteur,Ville_expediteur,Pays_expediteur,
                Telephone_expediteur,Mail_expediteur,cdes)
            
            SET `Remis_le`= STR_TO_DATE(@`Remis_le`,'%d/%m/%Y'),
                `Date_et_heure_de_l_evenement` = STR_TO_DATE(@`Date_et_heure_de_l_evenement`,'%d/%m/%Y') 
              
            ";
 $n=mysql_query($query);

try
{
    // On se connecte à MySQL
      $pdo = new PDO('mysql:host=localhost;port=8889;dbname=cadrage','root','root');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
      $rs = $pdo->query('SELECT * FROM `chronopost`');
           
    echo 'Il y a '.$rs->rowcount().' enregistrement(s) dans Chronopost.<br>';

 
?>

</body> 
</html>

