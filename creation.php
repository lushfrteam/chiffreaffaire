<?php
include("./config.php");
require_once("./Functions.php");
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

if ( (table_existe('order')>0) AND (table_existe('ogone')>0) AND (table_existe('chronopost')>0) AND (table_existe('novea')>0) )
    {
        echo "creation fichier<BR/>".$nom_fichier_excel;
        // Create new PHPExcel object
        echo date('H:i:s') , " Create new PHPExcel object" , EOL;
        $objPHPExcel = new PHPExcel();
        // Set document properties
        echo date('H:i:s') , " Set document properties" , EOL;
        $objPHPExcel->getProperties()->setCreator("LUSH")
							 ->setLastModifiedBy("LUSH")
							 ->setTitle("Cadrage CA")
							 ->setSubject("Source")
							 ->setDescription("Source donnees Cadrage")
							 ->setKeywords("cadrage")
							 ->setCategory("Test");
        // Add some data
        echo date('H:i:s') , " Add some data" , EOL;
        
        
        //conection base de données
        $bdd=connection_pdo();
        //requete
//        $requete="SELECT `order`.*, `ogone`.*, `chronopost`.*
          $requete="SELECT `ogone`.Id, `order`.order_id, `order`.date_added, 
              `ogone`.STATUS, `ogone`.LIB, `ogone`.ACCEPT, `ogone`.PAYDATE,
              `order`.firstname, `order`.lastname, 
              `ogone`.COUNTRY, `order`.Total_Inc_taxes, 
              `order`.currency, `order`.payment_method, `ogone`.METHODE,
              `ogone`.CARD, `ogone`.STRUCT, `order`.date_shipping,
              `order`.Sub_Total, `order`.Taxes, `order`.Shipping
               FROM `order`
               INNER JOIN `ogone` ON `order`.order_id = `ogone`.REF
               LEFT JOIN `chronopost` ON `order`.order_id = `chronopost`.`reference_expediteur`";
        
        $reponse = $bdd->query($requete);
        // On affiche chaque entrée une à une
        
       // $stmt = $pdo->query('SELECT * FROM `matable`');
        $tuples = $reponse->fetchAll(PDO::FETCH_ASSOC);
 
        if(count($tuples)) {
        $columns_names = array_keys($tuples[0]);
 
        echo '<table><thead><tr>';
        $i='A';
        foreach($columns_names as $col) {
            echo '<th>'. $col .'</th>';
             $objPHPExcel->getActiveSheet()->setCellValue($i.'1',$col);
            $i++;
        }
        echo '</tr></thead><tbody>';
        $i='2';
        foreach($tuples as $tuple) {
          echo '<tr>';
            $j='A';
            foreach($tuple as $col) {
                echo '<td>'. $col .'</td>';
                $objPHPExcel->getActiveSheet()->setCellValue($j.$i,$col);
                $j++;
            }
            $i++;
            echo '</tr>';
        }
    echo '</tbody></table>';
}
else {
    echo 'Pas de résultat';
}
        
        
        
        
        
       // $colcount = $reponse->columnCount();
       // while ($donnees = $reponse->fetch())
       // {echo $
            
            
       // }
        
    //    $objPHPExcel->setActiveSheetIndex(0)
    //        ->setCellValue('A1', 'Hello')
    //        ->setCellValue('B2', 'world!')
    //        ->setCellValue('C1', 'Hello')
    //        ->setCellValue('D2', 'world!');
        // Miscellaneous glyphs, UTF-8
   //     $objPHPExcel->setActiveSheetIndex(0)
   //         ->setCellValue('A4', 'Miscellaneous glyphs')
   //         ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
   //     $objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
        
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
        $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);
        // Rename worksheet
        echo date('H:i:s') , " Rename worksheet" , EOL;
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Save Excel 2007 file
        echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
   //     $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
             $objWriter->save(str_replace('.php', '.xlsx', $nom_fichier_excel));
        $callEndTime = microtime(true);
        $callTime = $callEndTime - $callStartTime;
        echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo($nom_fichier_excel, PATHINFO_BASENAME)) , EOL;
        echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
        // Echo memory usage
        echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;
        // Save Excel 95 file
        echo date('H:i:s') , " Write to Excel5 format" , EOL;
        $callStartTime = microtime(true);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
       // $objWriter->save(str_replace('.php', '.xls', __FILE__));
        $objWriter->save(str_replace('.php', '.xls', $nom_fichier_excel));
        $callEndTime = microtime(true);
        $callTime = $callEndTime - $callStartTime;
        echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo($nom_fichier_excel, PATHINFO_BASENAME)) , EOL;
        echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
        // Echo memory usage
        echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;
        // Echo memory peak usage
        echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
        // Echo done
        echo date('H:i:s') , " Done writing files" , EOL;
        echo 'Files have been created in ' , getcwd() , EOL;    
    
    }  

?>