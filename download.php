<?php
if (!isset($_GET["table"])) {
    echo "erreur";
    exit();
}
else {
    $table = $_GET["table"];
}


  // output headers so that the file is downloaded rather than displayed
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Description: File Transfer');
header("Content-type: text/csv");
header("Content-disposition: attachment; filename = ".$table.".csv");
include("./config.php");
require_once("./Functions.php");


//echo "<BR>table = ".$table."  <BR>";
$pdo=connection_pdo();
$pdo->exec("SET CHARACTER SET utf8");
 

// Creates a new csv file and store it in tmp directory
$new_csv = fopen("php://output", 'w');

//----- requete voir nom colonne
$query="SHOW COLUMNS FROM ".$table;
//echo "QUERY1 : ".$query;
$recordset = $pdo->query($query);
$fields = $recordset->fetchAll();
$lignes;
$i=0;

//----- exporter nom colonne
foreach ($fields as $field) {
    $ligne.=$field['Field'].',';
    $lignes[$i]=$field['Field'];
    $i=$i+1;
		
	}
$nb_colonnes=$i;        
fputcsv($new_csv,$lignes,$fieldseparator);

// ---- requete selection toute la base
$sql = "SELECT * FROM ".$table;
$traiter = $pdo->query($sql);
$records = $traiter->fetchAll(PDO::FETCH_ASSOC);
// export lignes
foreach($records as $fields):
fputcsv($new_csv, $fields,$fieldseparator);
endforeach;


fclose($new_csv);


?>