<html> 
<head> 
<title>Importer un fichier texte dans une bdd MySQL</title> 
</head> 
<body> 
<h2>Importer un fichier texte dans une bdd MySQL</h2> 

<?
 
// SERVEUR SQL
$sql_serveur="localhost:8889";

// LOGIN SQL
$sql_user="root";

// MOT DE PASSE SQL
$sql_password="root";

// NOM DE LA BASE DE DONNEES
$sql_database="cadrage";
// NOM DE LA TABLE DE DONNEES
$sql_table="ogone";

/****************************/
/**** connection database ***/
/****************************/
try
{
    // On se connecte à MySQL
      $pdo = new PDO('mysql:host=localhost;port=8889;dbname=cadrage','root','root');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

echo "connection ok<br>";

  /***************************/
 /* On cree la table  OGONE */
 /***************************/
 
  $query = "CREATE TABLE `ogone` (
				`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`REF` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
				`ORDER` DATETIME NOT NULL,
                                `STATUS` INT NOT NULL,
                                `LIB` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `ACCEPT` INT NOT NULL,
                                `PAYDATE` DATETIME NOT NULL,
                                `CIE` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `NAME` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `COUNTRY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `TOTAL` decimal(15,4) NOT NULL DEFAULT '0.0000',
                                `CUR` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `METHODE` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `BRAND` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `CARD` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
                                `STRUCT` INT NOT NULL 
    



   ) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;";
 
$pdo->prepare($query)->execute();
  
  /************************/
/*** import csv *********/
/************************/

$pdo->exec("LOAD DATA LOCAL INFILE '/Users/David/Documents/Sites Web/Cadrage/ogone.csv'
                                INTO TABLE `ogone`
                                FIELDS TERMINATED BY ';'
                                OPTIONALLY ENCLOSED BY ''
                                LINES TERMINATED BY '\r'
                                ");


  

?>

</body> 
</html>