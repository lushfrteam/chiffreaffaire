<?php

function ajouter_order_annuel()
{
    $pdo=connection_pdo();
    $req="INSERT IGNORE `annuel` SELECT * FROM `order`";
    $rs = $pdo->prepare($req);
    $rs->execute();
    $req="UPDATE `annuel` SET 
  `annuel`.`firstname`=`order`.`firstname`,
  `annuel`.`lastname`=`order`.`lastname`,
  `annuel`.`shipping_firstname`=`order`.`shipping_firstname`,
  `annuel`.`shipping_lastname`=`order`.`shipping_lastname`,
  `annuel`.`Products`=`order`.`Products`,
  `annuel`.`Sub_Total`=`order`.`Sub_Total`,
  `annuel`.`Shipping`=`order`.`Shipping`,
  `annuel`.`Payment`=`order`.`Payment`,
  `annuel`.`Other`=`order`.`Other`,
  `annuel`.`Coupon`=`order`.`Coupon`,
  `annuel`.`Discount`=`order`.`Discount`,
  `annuel`.`Voucher`=`order`.`Voucher`,
  `annuel`.`Taxes`=`order`.`Taxes`,
  `annuel`.`Total_Inc_Taxes`=`order`.`Total_Inc_Taxes`,
  `annuel`.`currency`=`order`.`currency`,  
  `annuel`.`payment_method`=`order`.`payment_method`,
  `annuel`.`shipping_method_id`=`order`.`shipping_method_id`,
  `annuel`.`date_added`=`order`.`date_added`,
  `annuel`.`date_shipping`=`order`.`date_shipping`,
  `annuel`.`periode_extract`=`order`.`periode_extract`,
   `annuel`.`commandes_payees`=`order`.`commandes_payees`,
   `annuel`.`montant_paye`=`order`.`montant_paye`,
   `annuel`.`type_paiement`=`order`.`type_paiement`

FROM `order` WHERE `annuel`.`order_id`=`order`.`order_id`;
";
    $rs = $pdo->prepare($req);
    $rs->execute();
    $pdo=null;
}

function ajouter_ogone_annuel()
{
    //echo "<BR>Ajouter Ogone Annuel<BR>";
    $pdo=connection_pdo();
    $req="INSERT IGNORE `ogone_annuel` SELECT * FROM `ogone`";
    $rs = $pdo->prepare($req);
    $rs->execute();
    $pdo=null;
}


function plus_ancien($table, $champs) {
    $pdo=connection_pdo();
    $req="SELECT max(`".$champs."`) FROM `".$table."`";
    $rs = $pdo->query($req);
    //$rs->execute();
    $donnees=$rs->fetchAll();
    $pdo=null;
    
    $date_ancien=$donnees[0][0];
    $formatter = new IntlDateFormatter('fr_FR',IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,'Europe/Paris',IntlDateFormatter::GREGORIAN );
    $ancien =DateTime::createFromFormat('Y-m-d', $date_ancien);
    return $formatter->format($ancien);
    
    }

function plus_recent($table, $champs) {
    $pdo=connection_pdo();
    $req="SELECT min(`".$champs."`) FROM `".$table."`";
    $rs = $pdo->query($req);
    //$rs->execute();
    $donnees=$rs->fetchAll();
    $pdo=null;
    
    $date_recent=$donnees[0][0];
    $formatter = new IntlDateFormatter('fr_FR',IntlDateFormatter::LONG,
        IntlDateFormatter::NONE,'Europe/Paris',IntlDateFormatter::GREGORIAN );
    $recent =DateTime::createFromFormat('Y-m-d', $date_recent);
    return $formatter->format($recent);
    
    //return $retour=$donnees[0][0]; 
}

function liste_table_archive(){
    global $sql_serveur;
    global $sql_user;
    global $sql_password;
    global $sql_database;
    global $sql_port; 
try 
            {
            $pdo = new PDO('mysql:host='.$sql_serveur.';port='.$sql_port.';dbname='.$sql_database,$sql_user,$sql_password);
            //$pdo = new PDO('mysql:host=localhost;port=3306;dbname=cadrage,root,20Olympian15');
            
            }
    catch(Exception $e)
            {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
            }
    $rs = $pdo->query("SHOW TABLES LIKE 'Archive%'");
    $records = $rs->fetchAll();
    
    
    return $records;
    
};


function nb_ligne($nom_table){
    global $sql_serveur;
    global $sql_user;
    global $sql_password;
    global $sql_database;
    global $sql_port; 
    try
            {
            $pdo = new PDO('mysql:host='.$sql_serveur.';port='.$sql_port.';dbname='.$sql_database,$sql_user,$sql_password);
            }
    catch(Exception $e)
            {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
            }
    $rs = $pdo->query('SELECT * FROM `'.$nom_table.'`');
    $retour=$rs->rowcount();
return $retour;
} 

function nb_remb_ogone($nom_table){
    global $sql_serveur;
    global $sql_user;
    global $sql_password;
    global $sql_database;
    global $sql_port; 
    try
            {
            $pdo = new PDO('mysql:host='.$sql_serveur.';port='.$sql_port.';dbname='.$sql_database,$sql_user,$sql_password);
            }
    catch(Exception $e)
            {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
            }
    $query="SELECT * FROM ogone WHERE (`STATUS`=7 or `STATUS`=8)";  
    $rs = $pdo->query($query);
    $retour=$rs->rowcount();
return $retour;
} 

function somme_total($nom_table, $champs){
    global $sql_serveur;
    global $sql_user;
    global $sql_password;
    global $sql_database;
    global $sql_port; 
    
    try
            {
            $pdo = new PDO('mysql:host='.$sql_serveur.';port='.$sql_port.';dbname='.$sql_database,$sql_user,$sql_password);
            }
    catch(Exception $e)
            {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
            }
    $rs = $pdo->query('SELECT SUM('.$champs.') AS TOTAL FROM `'.$nom_table.'`');
   //echo 'SELECT SUM('.$champs.') AS TOTAL FROM `'.$nom_table.'`';
    $donnees=$rs->fetch(PDO::FETCH_ASSOC);
    //echo $donnees['TOTAL'];
    
return $retour=$donnees['TOTAL']; 
    
}
function table_existe ($nom_table)
{
    global $sql_serveur;
    global $sql_user;
    global $sql_password;
    global $sql_database;
    global $sql_port; 
    $pdo = new PDO('mysql:host='.$sql_serveur.';port='.$sql_port.';dbname='.$sql_database,$sql_user,$sql_password);
$rs = $pdo->query("SHOW TABLES LIKE '".$nom_table."'");
//if($results->rowcount()>0){echo 'table exists';}

return $rs->rowCount();
}

function clicediteur($lien)
{
    window.open($liens);
}

function connection_db($sql_serveur, $sql_user, $sql_password,$sql_database)
{
    /* Connexion bdd */
    $mysql_link = mysql_connect($sql_serveur, $sql_user, $sql_password);
    mysql_selectdb($sql_database) or die("Connexion impossible");
   
   echo 'Connected successfully';
 
}

function connection_pdo() {
        global $sql_serveur;
    global $sql_server;
global $sql_user;
global $sql_password;
global $sql_database;
global $sql_port;

try
            {
    
    $pdo = new PDO('mysql:host='.$sql_server.';port='.$sql_port.';dbname='.$sql_database.'',$sql_user,$sql_password);
            }
    catch(Exception $e)
            {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
            }    
return $pdo;
}

function supprimer_table($table)
{
    $pdo=connection_pdo();
    $req="DROP TABLE ".$table."";
    $req = str_replace ("'", "`",$req);
    $delete = $pdo->prepare($req);
    $delete->execute();
    $pdo=null;
return;   
}

function renommer_table($ancien_nom, $nouveau_nom)
{
    $pdo=connection_pdo();
    $req="RENAME TABLE ".$ancien_nom." TO ".$nouveau_nom;
    $delete = $pdo->prepare($req);
    $delete->execute();
    $pdo=null;
return;   
}

function rediriger_page($page){
      header('Location: ./');
  exit();
}

function execution_requete($requete, $connection) {
    $connection->exec($requete);
return;
}

Function fusion() {
    global $fichier_requetes;  
    global $content_dir;
    global $fieldseparator;
    global $nom_table_fusion;
    
    //ouverture base
    $pdo=connection_pdo();
    $pdo->exec("SET CHARACTER SET utf8");
    echo "<BR>fichier requetes : ".$fichier_requetes." <BR>";
    // on ouvre le fichier requete
    $monfichier = fopen($fichier_requetes, 'r');
    $nb_requete=0;
    /*Si on a réussi à ouvrir le fichier*/
    if ($monfichier)
    {
       	/*Tant que l'on est pas à la fin du fichier*/
	while (!feof($monfichier))
	{
                
		/*On lit la ligne courante*/
		$traitement = fgets($monfichier);
                /*On traite la ligne*/
                $traiter = $pdo->prepare($traitement);
                $traiter->execute();
                $nb_requete=$nb_requete+1;
	}
	/*On ferme le fichier*/
	fclose($monfichier);
    }
    
    $pdo=connection_pdo();

    // -------------------------------
    // --- export csv
    // --------------------------------
    // 
    //----- Ouverture Fichier ---------
//    $filename = $content_dir."/db_user_export_".time().".csv";
//    $handle = fopen($filename, 'w+');

    //----- requete voir nom colonne
//    $query="SHOW COLUMNS FROM ".$nom_table_fusion;
//    $recordset = $pdo->query($query);
//    $fields = $recordset->fetchAll();
//    $lignes;
//    $i=0;

    //----- exporter nom colonne
//    foreach ($fields as $field) {
//        $ligne.=$field['Field'].',';
//        $lignes[$i]=$field['Field'];
//        $i=$i+1;
//    }
//    $nb_colonnes=$i;        
  
    // ---- requete selection toute la base
//    $sql = "SELECT * FROM ".$nom_table_fusion;
//    $traiter = $pdo->query($sql);
//    $records = $traiter->fetchAll(PDO::FETCH_ASSOC);

    // export lignes
   // foreach($records as $fields):
      //  fputcsv($handle, $fields,$fieldseparator);
  //  endforeach;

    fclose($handle);
return; 
}

function test() {
    echo "<BR>test <BR>";
}
?>

