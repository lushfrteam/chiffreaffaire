
<?php

include("./config.php");
require_once("./Functions.php");
//header('Content-Type: text/csv; charset=utf-8');
//header('Content-Disposition: attachment; filename=data.csv');
//echo "Creation table ".$nom_table_fusion;

//ouverture base
$pdo=connection_pdo();
$pdo->exec("SET CHARACTER SET utf8");
        
// on ouvre le fichier requete

$monfichier = fopen($fichier_requetes, 'r');
$nb_requete=0;
/*Si on a réussi à ouvrir le fichier*/
if ($monfichier)
{
	/*Tant que l'on est pas à la fin du fichier*/
	while (!feof($monfichier))
	{
		/*On lit la ligne courante*/
		$traitement = fgets($monfichier);
		/*On traite la ligne*/
                $traiter = $pdo->prepare($traitement);
                $traiter->execute();
                $nb_requete=$nb_requete+1;
		//echo "<BR>".$traitement;
	}
	/*On ferme le fichier*/
	fclose($monfichier);
}
//$nb_requete=$nb_requete-1;
//echo "Nombre de requetes = ".$nb_requete."<BR>";

$pdo=connection_pdo();

// -------------------------------
// --- export csv
// --------------------------------
// 
//----- Ouverture Fichier ---------
$filename = $content_dir."/db_user_export_".time().".csv";
$handle = fopen($filename, 'w+');
//$handle = fopen('php://output','w+');

//----- requete voir nom colonne
$query="SHOW COLUMNS FROM ".$nom_table_fusion;
$recordset = $pdo->query($query);
$fields = $recordset->fetchAll();
$lignes;
$i=0;

//----- exporter nom colonne
foreach ($fields as $field) {
    $ligne.=$field['Field'].',';
    $lignes[$i]=$field['Field'];
    $i=$i+1;
		
	}
$nb_colonnes=$i;        
fputcsv($handle,$lignes,$fieldseparator);


// ---- requete selection toute la base
$sql = "SELECT * FROM ".$nom_table_fusion;
//$traiter = $pdo->prepare($sql);
//$traiter->execute();
$traiter = $pdo->query($sql);
//$results = $traiter->fetchAll();
$records = $traiter->fetchAll(PDO::FETCH_ASSOC);

// Reprise des resultats
//$result = $traiter->fetchall();
//$nb_lignes=$traiter->rowcount();

// Affichage nombre de ligne trouvees
//echo 'Result2 : ' . $nb_lignes . " lignes\n"; 

// export lignes
foreach($records as $fields):
fputcsv($handle, $fields,$fieldseparator);
endforeach;

fclose($handle);

 ?>        

